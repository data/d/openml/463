# OpenML dataset: backache

https://www.openml.org/d/463

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Data file:
This data from "Problem-Solving" on "backache in pregnancy"
is in somewhat different
format from that listed in the book. Each integer is preceded by a space.
This makes it easier to read. Each line is split in two separated by an
ampersand. Each line also has a full stop (or period) at the end of each
line which should be removed. If you have any queries, please contact me.
Chris Chatfield.
cc@maths.bath.ac.uk


Information about the dataset
CLASSTYPE: nominal
CLASSINDEX: last

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/463) of an [OpenML dataset](https://www.openml.org/d/463). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/463/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/463/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/463/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

